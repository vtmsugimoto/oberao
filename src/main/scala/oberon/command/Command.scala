package oberon.command

import oberon.Environment._
import parser._

import oberon.expression.Expression
import oberon.expression._
import oberon.expression.BoolValue
import br.unb.cic.ed.immutable.List
import br.unb.cic.ed.immutable.Hash
import br.unb.cic.ed.immutable._
import oberon.visitor._

trait Command {
  def run() : Unit 
}
sealed trait Chamavel

class BlockCommand(val cmds: List[Command]) extends Command {
  override
  def run() : Unit = {
      push()
      //var ret: Command = new Return(Undefined())
      List.map(cmds, (c:Command) => {
        (c,retorno) match {
          case (a:Return,false) => {c.run();retorno = true}
          case (_,false) => c.run()
          case (_,true) => {}
        }
      })
      pop()
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class Return(val ret: Expression) extends Command{
  def run(): Unit = {
    val retAss = new Assignment("0", ret)
    retAss.run()
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class Declaration(val tipo: String, val id: String, val expression: Expression) extends Command {
  def run(): Unit = {
    map(id, expression)
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class Assignment(val id: String, val expression: Expression, val external: Boolean = false) extends Command {
  override
  def run() : Unit = {
    map(id, expression, external)
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class While(val cond: Expression, val command: Command) extends Command {
  override
  def run() : Unit = {
    val v = cond.eval.asInstanceOf[BoolValue]
    v.value match {
      case true => {
        (command,retorno) match {
          case (a:Return,false) => {command.run();retorno = true}
          case (_,false) => {command.run();this.run()}
          case (_,true) => {}
        }
      }
      case _               => { } 
    }
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class IfThen(val cond: Expression, val command: Command) extends Command {
  def run(): Unit = {
      cond.eval.asInstanceOf[BoolValue].value match {
        case true => {
          (command,retorno) match {
            case (a:Return,false) => {command.run();retorno = true}
            case (_,false) => command.run()
            case (_,true) => {}
          }
        }
        case _ => {}
      }
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class IfThenElse(val cond: Expression, val commandTrue: Command, val commandFalse: Command) extends Command{
  def run(): Unit = {
    cond.eval.asInstanceOf[BoolValue].value match {
      case true => {
          (commandTrue,retorno) match {
            case (a:Return,false) => {commandTrue.run();retorno = true}
            case (_,false) => commandTrue.run()
            case (_,true) => {}
          }
        }
      case false => {
          (commandFalse,retorno) match {
            case (a:Return,false) => {commandFalse.run();retorno = true}
            case (_,false) => commandFalse.run()
            case (_,true) => {}
          }
        }
    }
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
//pop()
class DecProcedure(val id: String, val foo: Procedure) extends Command{
  def run(): Unit = {
    insertCall(id, foo)
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
class DecFunction(val id: String, val foo: Function) extends Command{
  def run(): Unit = {
    insertCall(id, foo)
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
case class Parametro(val tipo: String, val nome: String)
case class Procedure(val parametros: List[Parametro], val comandos: Command) extends Chamavel
case class Function(val parametros: List[Parametro], val comandos: Command, val retorno: String) extends Chamavel

class CallProcedure(val id: String, val input: Stack[Expression]) extends Command{
  def run(): Unit = {
    val foo = getCall(id)
    var parEn: Stack[Expression] = Stack()
    var aux: Stack[Expression] = Stack()
    aux = Stack.pushAll(input, aux)
    while(Stack.size(parEn) != Stack.size(input)){
      parEn = Stack.push(Stack.top(aux).eval(),parEn)
      aux = Stack.pop(aux)
    }
    parEn = Stack.reverse(parEn)
    //parEn = Stack.pushAll(input, parEn)
    startCall()
    List.map(foo.asInstanceOf[Procedure].parametros,
      (par:Parametro) => {
        var dec = new Declaration(par.tipo, par.nome, Stack.top(parEn))
        dec.run()
        parEn = Stack.pop(parEn)
      })
    foo.asInstanceOf[Procedure].comandos.run()
    endCall()
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class Print(val exp: Expression) extends Command {
  override
  def run() : Unit = {
    val a = exp.eval()
    val pp = new PrettyPrinter()
    pp.visit(this)
    println(pp.str)
    pp.visit(a)
    println("Valor da expressão: " + pp.str)
    //println("Print: "+exp+" -> "+exp.eval())
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
