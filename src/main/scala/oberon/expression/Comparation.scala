package oberon.expression

import oberon.visitor._

abstract class Comparation(val lhs: Expression, val rhs: Expression) extends IntExpression

class GtExpression(lhs: Expression, rhs: Expression) extends Comparation(lhs, rhs){
  def eval(): Value = new BoolValue(IntExt(lhs).value > IntExt(rhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class LtExpression(lhs: Expression, rhs: Expression) extends Comparation(lhs, rhs){
  def eval(): Value = new BoolValue(IntExt(lhs).value < IntExt(rhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class DifExpression(lhs: Expression, rhs: Expression) extends Comparation(lhs, rhs){
  def eval(): Value = new BoolValue(IntExt(lhs).value != IntExt(rhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class GteExpression(lhs: Expression, rhs: Expression) extends Comparation(lhs, rhs){
  def eval(): Value = new BoolValue(IntExt(lhs).value >= IntExt(lhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
class LteExpression(lhs: Expression, rhs: Expression) extends Comparation(lhs, rhs) {
  def eval: Value = new BoolValue(IntExt(lhs).value <= IntExt(lhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
class EqExpression(lhs: Expression, rhs: Expression) extends Comparation(lhs, rhs) {
  def eval: Value = new BoolValue(IntExt(lhs).value == IntExt(rhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}