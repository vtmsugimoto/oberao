package oberon.expression

import oberon.visitor._

trait Expression {
  def eval(): Value 
}

trait Value extends Expression {
  def eval() = this 
}

trait IntExpression extends Expression
trait BoolExpression extends Expression

abstract class OutValue(value: Value) extends Value {
	override def eval() = value.eval()
}

class Undefined() extends Value{
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
class OutOfScope(val value: Value) extends OutValue(value){
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this.value)
}
class IntValue(val value: Integer) extends Value{
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
class BoolValue(val value: Boolean) extends Value{
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class NotExpression(val exp: Expression) extends Expression{
	def eval(): Value = new BoolValue(!exp.eval().asInstanceOf[BoolValue].value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

object IntExt{
  def apply(v: Expression): IntValue =
     v.eval() match {
      case a:IntValue => a
      case a:OutOfScope => a.value.asInstanceOf[IntValue]
    }
}

object BoolExt{
  def apply(v: Expression): BoolValue = 
    v.eval() match {
      case a:BoolValue => a
      case a:OutOfScope => a.value.asInstanceOf[BoolValue]
    }
}