package oberon.expression

import oberon.Environment._
import oberon.command._
import oberon.expression._
import br.unb.cic.ed.immutable._
import oberon.visitor._

class CallFunction(val id: String, val input: List[Expression]) extends Expression{
	def eval(): Value = {
    val foo = getCall(id)
    var parEn: Stack[Expression] = Stack()
    var aux: Stack[Expression] = Stack()
    aux = Stack.pushAll(input, aux)
    while(Stack.size(parEn) != Stack.size(input)){
      parEn = Stack.push(Stack.top(aux).eval(),parEn)
      aux = Stack.pop(aux)
    }
    parEn = Stack.reverse(parEn)
    //parEn = Stack.pushAll(input, parEn)
    startCall()
    val retDec = new Declaration(foo.asInstanceOf[Function].retorno, "0", new Undefined())
    retDec.run()
    List.map(foo.asInstanceOf[Function].parametros,
      (par:Parametro) => {
        var dec = new Declaration(par.tipo, par.nome, Stack.top(parEn))
        dec.run()
        parEn = Stack.pop(parEn)
      })
    foo.asInstanceOf[Function].comandos.run()
    val retRef = new VarRef("0")
    val retValue = retRef.eval()
    endCall()
    retorno = false
    return retValue
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}