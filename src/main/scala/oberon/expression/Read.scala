package oberon.expression
import oberon.visitor._

class ReadInt() extends Expression{
	def eval(): Value = new IntValue(scala.io.StdIn.readInt())
	def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class ReadBool() extends Expression{
	def eval(): Value = new BoolValue(scala.io.StdIn.readBoolean())
	def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
