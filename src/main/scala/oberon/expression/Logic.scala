package oberon.expression
import oberon.visitor._

abstract class Logic( val lhs: Expression, val rhs: Expression) extends BoolExpression

class AndExpression(lhs: Expression, rhs: Expression) extends Logic(lhs, rhs){
  def eval(): Value = new BoolValue(lhs.eval().asInstanceOf[BoolValue].value && rhs.eval().asInstanceOf[BoolValue].value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class OrExpression(lhs: Expression, rhs: Expression) extends Logic(lhs, rhs){
  def eval(): Value = new BoolValue(lhs.eval().asInstanceOf[BoolValue].value || rhs.eval().asInstanceOf[BoolValue].value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

/*
private def myFoo(lhs: Expression, rhs: Expression, foo: (Boolean, Boolean) => Boolean): Value =
  BoolValue(foo(IntExt(lhs).value, IntExt(rhs).value))

  def eval: Value = myFoo(lhs, rhs, _ == _)*/
