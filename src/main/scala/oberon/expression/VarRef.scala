package oberon.expression

import oberon.Environment._
import oberon.visitor._

class VarRef(val id: String, val fromCall: Boolean = false) extends Expression {
  override
  def eval() : Value = lookup(id, false) match {
  	case Right(v) => v
    case Left(Some(v)) => new OutOfScope(v)
    case Left(None) => new Undefined()   
  }
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}
