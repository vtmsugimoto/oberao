package oberon.expression
import oberon.visitor._

abstract class Operation(val lhs: Expression, val rhs: Expression) extends IntExpression

class AddExpression(lhs: Expression, rhs: Expression) extends Operation(lhs, rhs) {
  def eval() : Value = new IntValue(IntExt(lhs).value + IntExt(rhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class SubExpression(lhs: Expression, rhs: Expression) extends Operation(lhs, rhs){
  def eval(): Value = new IntValue(IntExt(lhs).value - IntExt(rhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class ModExpression(lhs: Expression, rhs: Expression) extends Operation(lhs, rhs){
  def eval(): Value = new IntValue(IntExt(lhs).value % IntExt(rhs).value) 
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class DivExpression(lhs: Expression, rhs: Expression) extends Operation(lhs, rhs){
  def eval(): Value = new IntValue(IntExt(lhs).value / IntExt(rhs).value)
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}

class MulExpression(lhs: Expression, rhs: Expression) extends Operation(lhs, rhs){
  def eval(): Value = new IntValue(IntExt(lhs).value * IntExt(rhs).value)  
  def accept(okyaku_san: Visitor) = okyaku_san.visit(this)
}