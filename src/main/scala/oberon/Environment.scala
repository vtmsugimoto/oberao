package oberon

import scala.collection.mutable.Map

import oberon.expression.Value
import oberon.expression.Expression
import br.unb.cic.ed.immutable.Stack._
import br.unb.cic.ed.immutable._

object Environment {
  var stack: Stack[Hash[String, Value]] = Stack()
  var id_tipo: Stack[Hash[String, String]] = Stack()
  var backupId_Tipo: Stack[Stack[Hash[String, String]]] = Stack()
  var backupAmbiente: Stack[Stack[Hash[String, Value]]] = Stack()
  var funcoes: Hash[String, oberon.command.Chamavel] = Hash()
  var retorno: Boolean = false

  def startCall(): Unit = {
    val backup: Stack[Hash[String,Value]] = Stack()
    backupAmbiente = Stack.push(Stack.pushAll(stack, backup), backupAmbiente)
    stack = Stack()
    push()
  }

  def endCall(): Unit = {
    stack = Nil
    stack = Stack.pushAll(Stack.top(backupAmbiente), stack)
    backupAmbiente = Stack.pop(backupAmbiente)
    //stack = Stack.pushAll(backupAmbiente, stack)
  }

  def insertCall(id: String, foo: oberon.command.Chamavel) = {
    HashMap.insert(id, foo, funcoes)
  }

  def getCall(id: String): oberon.command.Chamavel = {
    HashMap.get(id, funcoes)
  }
  def push() {
    stack = Stack.push(HashMap[String, Value](), stack)
  }

  def pop() {
    stack = Stack.pop(stack)
  }

  def map(id: String, value: Expression, external: Boolean = false) {
    if(Stack.size(stack) == 0) {
      push()
    }
    var pilha:Stack[Hash[String,Value]] = Stack()
    pilha = Stack.pushAll(stack, pilha)
    if(true){
      var pilhaSize = Stack.size(pilha)
      while(pilhaSize > 1 && !HashMap.exist(id,Stack.top(pilha))){
        pilhaSize += -1
        pilha = Stack.pop(pilha)
      }
    }
    HashMap.insert(id,value.eval(),Stack.top(pilha))
  }

  def lookup(id: String, lockTop: Boolean = false) : Either[Option[Value],Value] = {
    if(!lockTop){
      if(Stack.size(stack) != 0){
        //println(Stack.size(stack))
        var pilha:Stack[Hash[String,Value]] = Stack()
        pilha = pushAll(stack, pilha)
        lookdown(id, pilha)
      }else{
         lookdown(id, stack)
      }
    }else{
      if(size(stack) > 0 && HashMap.exist(id, top(stack))) Right(HashMap.get(id, top(stack)))
      else Left(None)
    }
  }

  private def lookdown(id: String, pilha: Stack[Hash[String, Value]], topo: Boolean = true): Either[Option[Value], Value] = {
    if(size(pilha) == 0){// || (size(stack) != 0 && !HashMap.exist(id, top(stack)))){
      Left(None)
    }else{
      if(HashMap.exist(id, top(pilha))){
        if(topo) Right(HashMap.get(id, top(pilha)))
        else Left(Some(HashMap.get(id, top(pilha))))
      }else{
        lookdown(id, Stack.pop(pilha), false)
      }
    }
  }

  def clear() : Unit = { stack = Stack(); funcoes = Hash() } 
}
