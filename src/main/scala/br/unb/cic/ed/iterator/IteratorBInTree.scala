package br.unb.cic.ed.iterator
import br.unb.cic.ed.mutable._
import br.unb.cic.ed.immutable._

class IteratorBinTree[A <: Comparable[A]](tree: br.unb.cic.ed.mutable.BinTreeImpl[A]) extends Iterable[A]{

	private var current: NodeTree[A] = tree.getRoot()
	private var parent: br.unb.cic.ed.immutable.Stack[NodeTree[A]] = br.unb.cic.ed.immutable.Stack()
	private var pos: Int = 0
	private var finished: Boolean = false

  	def hasNext(): Boolean = tree.size() - pos != 1

  	def next(): Unit = {
  		if(hasNext()){
  			current match{
  				case node:NodeTree[A] if node.lhs != null => {
  					parent = Stack.push(current, parent)
  					current = node.lhs
  				}
  				case node:NodeTree[A] if node.rhs != null => {
  					parent = Stack.push(current, parent)
  					current = node.rhs
  				}
  				case node:NodeTree[A] if (node.rhs == null && node.lhs == null) => {
  					var aux: NodeTree[A] = Stack.top(parent)
  					while(aux.rhs == current || aux.rhs == null){
  						current = Stack.top(parent)
  						parent = Stack.pop(parent)
  						aux = Stack.top(parent)
  					}
  					current = aux.rhs
  				}
  			}
  			pos += 1
  		}else{
  			finished = true
  		}
  	}

  	def get(): A = {
  		val ret = current.content
  		next()
  		return ret
  	}

	def see(): A = current.content

	def getNode(): NodeTree[A] = current

	def getParent(): NodeTree[A] = Stack.top(parent)

	def getPos(): Int = pos

  	def hasFinished(): Boolean = finished




}