package br.unb.cic.ed.iterator

trait Iterable[T]{
  	def hasNext(): Boolean
  	def next(): Unit
  	def get(): T
	def see(): T
	def getPos(): Int
  	def hasFinished(): Boolean
}
