package br.unb.cic.ed.iterator
import br.unb.cic.ed._

class IteratorLinkedList[T](private val that: br.unb.cic.ed.mutable.LinkedList[T])extends br.unb.cic.ed.iterator.Iterable[T]{

  private var iterator: br.unb.cic.ed.mutable.NodeList[T] = that.nodeAtPosition(0)  
  private var index: Int = 0
  private var finished: Boolean = false

  def get(): T = {
    val ret: T = iterator.value
    next()
    return ret
  }
  
  def see(): T = iterator.value

  def getPos(): Int = index

  def hasNext(): Boolean = {
    if (index < (that.size() - 1) ){
      return true
    }else{
      return false
    }
  }
  def next(): Unit = {
    if (this.hasNext()){
      index = index + 1
      iterator = iterator.next
    }else{
      finished = true
    }
  }

  def hasFinished(): Boolean = return finished
}
