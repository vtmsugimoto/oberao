package br.unb.cic.ed.iterator
import br.unb.cic.ed.immutable._

class IteratorImmutableStack[A](first: Stack[A]) extends Iterable[A]{

	private var current = first
	private var pos: Int = 0
	private var finished: Boolean = false

	def hasNext(): Boolean = (Stack.pop(current) != Nil)

	def next(): Unit = 
		if(hasNext){
			current = Stack.pop(current)
			pos += 1
		}else{
			finished = true
		}

	def get(): A = {
		val ret = Stack.top(current)
		next()
		return ret
	}

	def see(): A = Stack.top(current)

	def getPos(): Int = pos

	def hasFinished(): Boolean = finished || Stack.size(current) == 0



}
