package br.unb.cic.ed.iterator
import br.unb.cic.ed.immutable._

class IteratorImmutableBinTree[A <: Comparable[A]](root: BinTree[A], mode: Int = 0) extends Iterable[A]{

	private var current: BinTree[A] = root
	private var parent: br.unb.cic.ed.immutable.Stack[BinTree[A]] = br.unb.cic.ed.immutable.Stack()
	private var pos: Int = 0
	private var finished: Boolean = false


  	def hasNext(): Boolean = BinTree.size(root) - pos != 1

  	def next(): Unit = {
  		if(hasNext()){
  			current match{
  				case Node(_,l: BinTree[A],_) 	=> {
  					parent = Stack.push(current,parent)
  					current = l
  				}
  				case Node(_,_,r: BinTree[A]) 	=> { 
  					parent = Stack.push(current,parent)
  					current = r
  				}
  				case Node(_,null,null) => {
  					var aux: BinTree[A] = Stack.top(parent)
  					while(aux.asInstanceOf[Node[A]].right == current || aux.asInstanceOf[Node[A]].right == null){
  						current = Stack.top(parent)
  						parent 	= Stack.pop(parent)
  						aux 	= Stack.top(parent)
  					}
  					current = aux.asInstanceOf[Node[A]].right
  				}
  			}
  			pos += 1
  		}else{
  			finished = true
  		}
  	}

  	def get(): A = {
  		val ret: A = BinTree.value(current)
  		next()
  		return ret
  	}
	def see(): A = BinTree.value(current)

	def getPos(): Int = pos

  	def hasFinished(): Boolean = finished
  }