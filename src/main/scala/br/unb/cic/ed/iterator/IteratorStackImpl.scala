package br.unb.cic.ed.iterator
import br.unb.cic.ed.mutable._

class IteratorStackImpl[A](current: Stack[A]) extends br.unb.cic.ed.iterator.Iterable[A]{

	private var element: A = _
	private var pos: Int = 0
	private var finished: Boolean = false

	def hasNext(): Boolean = current.size() > 1

  	def next(): Unit = 
  		if(hasNext()){
  			current.pop()
  			pos += 1
  		}else{
  			finished = true
  		}

  	def get(): A = {
  		val ret = current.top()
  		next()
  		ret match {
  			case Some(x) 	=> x
  			case None 		=> throw new br.unb.cic.ed.mutable.InvalidArgument("Pilha vazia")
  		}
  	}

	def see(): A = 
		current.top match {
			case Some(x) => x
			case None	 => throw new br.unb.cic.ed.mutable.InvalidArgument("Pilha vazia")
		}

  	def getPos(): Int = pos

  	def hasFinished(): Boolean = finished


}