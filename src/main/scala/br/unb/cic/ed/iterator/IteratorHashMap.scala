package br.unb.cic.ed.iterator

class IteratorHashMap[A,B](hash: br.unb.cic.ed.mutable.HashMap[A,B], keyList: br.unb.cic.ed.immutable.List[A]) extends Iterable[B]{

	private val itList: br.unb.cic.ed.iterator.Iterable[A] = br.unb.cic.ed.immutable.List.iterator(keyList)
	private var current: B = hash.get(itList.see())
	private var pos: Int = 0
	private var finished: Boolean = false

  	def hasNext(): Boolean = pos < hash.size() - 1

  	def next(): Unit = {
  		if(hasNext()){
  			itList.next()
  			current = hash.get(itList.see())
  		}else{
  			finished = true
  		}
  	}

  	def get(): B = {
  		val ret = current
  		next()
  		return ret
  	}

	def see(): B = current

	def getPos(): Int = pos

  	def hasFinished(): Boolean = finished


}