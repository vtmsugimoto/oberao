package br.unb.cic.ed.iterator
import br.unb.cic.ed.immutable._

class IteratorImmutableList[A](first: br.unb.cic.ed.immutable.List[A]) extends Iterable[A]{

	private var current = first
	private var pos: Int = 0
	private var finished: Boolean = false

	def hasNext(): Boolean = 
		current match{
			case Cons(x,Nil)=> false
			case Nil		=> false
			case Cons(x,xs)	=> true
		}

	def next(): Unit = {
		current match{
			case Cons(x,xs) if hasNext() 	=> current = xs
			case _ 							=> finished = true
		}
		pos += 1
	}
	def get(): A =
		current match{
			case Cons(x,xs) => {
								next()
								return x	
								}
			case Nil		=> throw br.unb.cic.ed.mutable.InvalidArgument("List is Nil")
		}

	def see(): A =
		current match{
			case Cons(x,xs) => x
			case Nil		=> throw br.unb.cic.ed.mutable.InvalidArgument("List is Nil")
		} 

	def getPos(): Int = pos

	def hasFinished(): Boolean = finished



}
