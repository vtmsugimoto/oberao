package br.unb.cic.ed.iterator
import br.unb.cic.ed._

class IteratorList[T](private val that: br.unb.cic.ed.mutable.List[T])extends Iterable[T]{

  private var iterator: T = setIterator()
  private var index: Int = 0
  private var finished = false

  private[this] def setIterator(): T = {
      that.elementAt(index) match{
        case Some(value) => {
          iterator = value
          return value
        }
        case _           => throw new br.unb.cic.ed.mutable.InvalidArgument("problemas pra setar o iterator, hein")
      }
  }

  def get(): T = {
    val ret: T = iterator
    next()
    return ret
  }

  def see(): T = iterator

  def getPos(): Int = index

  def hasNext(): Boolean = {
    if (index < (that.size() - 1) ){
      return true
    }else{
      return false
    }
  }
  def next(): Unit = {
    if (this.hasNext()){
      index = index + 1
      setIterator()
    }else{
      finished = true
    }
  }

  def hasFinished(): Boolean = return finished
}
