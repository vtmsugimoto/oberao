package br.unb.cic.ed.iterator

class IteratorArrayList[T](private val that: br.unb.cic.ed.mutable.List[T])extends br.unb.cic.ed.iterator.Iterable[T]{

  private var iterator: T = setIterator()
  private var index: Int = 0
  private var finished: Boolean = false

  private def setIterator(): T = {
      that.elementAt(index) match{
        case Some(value) => {
          iterator = value
          return value
        }
        case _           => throw new br.unb.cic.ed.mutable.InvalidArgument("problemas pra setar o iterator, hein")
      }
  }

  def get(): T = {
    val ret: T = iterator
    next()
    return ret
  }

  def see(): T = iterator

  def getPos(): Int = index

  def hasNext(): Boolean = index < (that.size() - 1)
  
  def next(): Unit = {
    if (this.hasNext()){
      index = index + 1
      setIterator()
    }else{
      finished = true
    }
  }

  def hasFinished(): Boolean = return finished
}
