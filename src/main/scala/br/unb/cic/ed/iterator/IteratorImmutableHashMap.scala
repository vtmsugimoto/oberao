package br.unb.cic.ed.iterator

class IteratorImmutableHashMap[A,B](hash: br.unb.cic.ed.immutable.Hash[A,B], keyList: br.unb.cic.ed.immutable.List[A]) extends Iterable[B]{

	private val itList: br.unb.cic.ed.iterator.Iterable[A] = br.unb.cic.ed.immutable.List.iterator(keyList)
	private var current: B = br.unb.cic.ed.immutable.HashMap.get(itList.see(),hash)
	private var pos: Int = 0
	private var finished: Boolean = false

  	def hasNext(): Boolean = pos < br.unb.cic.ed.immutable.HashMap.size(hash) - 1

  	def next(): Unit = {
  		if(hasNext()){
  			itList.next()
  			current = br.unb.cic.ed.immutable.HashMap.get(itList.see(),hash)
  		}else{
  			finished = true
  		}
  	}

  	def get(): B = {
  		val ret = current
  		next()
  		return ret
  	}

	def see(): B = current

	def getPos(): Int = pos

  	def hasFinished(): Boolean = finished


}