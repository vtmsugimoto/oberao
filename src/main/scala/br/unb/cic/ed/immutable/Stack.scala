package br.unb.cic.ed.immutable

trait Stack[+A]

object Stack{
	def push[A](value: A, stack: Stack[A]): Stack[A] = {
		val list: List[A] = stack match {
			case x:List[A] => x
			case _			=> throw new br.unb.cic.ed.mutable.InvalidArgument("Dá certo não") 
		}
		stack match {
			case Nil 		=> Cons(value, Nil)
			case Cons(x,xs) => Cons(value, list)
		}
	}

	def pop[A](stack: Stack[A]): Stack[A] =
		stack match {
			case Nil 		=> Nil
			case Cons(x,xs) => xs
		}

	def top[A](stack: Stack[A]): A = 
		stack match {
			case Nil => throw new br.unb.cic.ed.mutable.InvalidArgument("Pilha vazia")
			case Cons(x,xs) => x
		}

	def pushAll[A](source: Stack[A], target: Stack[A]): Stack[A] = {
		var reversedSource = Stack.reverse(source)
		var ret = target
		val it = iterator(reversedSource)

		while(!it.hasFinished){
			ret = Stack.push(it.get(), ret)
		}

		return ret
	}

	def popAll[A](qty: Int, stack: Stack[A]): Stack[A] =
		Stack.size(stack) match {
			case _ if qty == 0 	=> stack
			case x if qty < 0 	=> throw new br.unb.cic.ed.mutable.InvalidArgument("Primeiro parâmetro inválido")
			case 0 				=> stack
			case x if x > 0 	=> popAll(qty-1,pop(stack))
		}

	def reverse[A](stack: Stack[A]): Stack[A] = {
		var reversed:Stack[A] = Nil
		val it = iterator(stack)

		while(!it.hasFinished){
			reversed = Stack.push(it.get(), reversed)
		}
		return reversed
	}

	def iterator[A](stack: Stack[A]): br.unb.cic.ed.iterator.Iterable[A] = new br.unb.cic.ed.iterator.IteratorImmutableStack(stack)

	def size[A](stack: Stack[A]): Int = 
		stack match {
			case Nil 		=> 0
			case Cons(x,xs) => 1 + size(xs)
		}

	def apply[A](values: A*): Stack[A] = {
		if(values.isEmpty) Nil
		else Cons(values.head: A, apply(values.tail: _*).asInstanceOf[List[A]])
	}
}
