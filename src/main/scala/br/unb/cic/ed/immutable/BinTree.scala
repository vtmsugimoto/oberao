package br.unb.cic.ed.immutable

trait BinTree[A <: Comparable[A]]

case class Node[A <: Comparable[A]](value: A, left: BinTree[A] = null, right: BinTree[A] = null) extends BinTree[A]

object BinTree{

	def insert[A <: Comparable[A]](value: A, root: BinTree[A]): BinTree[A] =
		root match {
			case Node(a,null,c) if value.compareTo(a) < 0 => {
				Node(a,Node(value,null,null),c)
			}
			case Node(a,c,null) if value.compareTo(a) > 0 => {
				Node(a,c,Node(value,null,null))
			}
			case Node(a,b,c) if value.compareTo(a) < 0 => Node(a,insert(value, b),c)
			case Node(a,c,b) if value.compareTo(a) > 0 => Node(a,c,insert(value, b))
			case null => {
				Node(value,null,null)
			}
			case _ => root
		}

	def size[A <: Comparable[A]](root: BinTree[A]): Int =
		root match {
			case null => 0
			case Node(_,a,b) => 1 + size(a) + size(b)
		}

	def value[A <: Comparable[A]](root: BinTree[A]): A = 
		root match {
			case Node(a,_,_)	=> a
			case null 			=> throw new br.unb.cic.ed.mutable.InvalidArgument("Árvore vazia")
		}

	def remove[A <: Comparable[A]](value: A, root: BinTree[A]): BinTree[A] = {
		if(!exist(value,root)){
			return root
		}
		val it = iterator(root)
		var ret: BinTree[A] = BinTree()
		while(!it.hasFinished()){
			if(it.see().compareTo(value) != 0){
				ret = insert(it.get(), ret)
			}else{
				it.get()
			}
		}
		return ret
	}
		
	def exist[A <: Comparable[A]](value: A, root: BinTree[A]): Boolean = 
		root match {
			case null => false
			case Node(valor,_,right) if valor.compareTo(value) < 0 => exist(value, right)
			case Node(valor,left,_) if valor.compareTo(value) > 0  => exist(value, left)
			case _ => true
		}

	def iterator[A <: Comparable[A]](root: BinTree[A], mode: Int = 0): br.unb.cic.ed.iterator.Iterable[A] = new br.unb.cic.ed.iterator.IteratorImmutableBinTree(root,mode)

	def apply[A <: Comparable[A]](values: A*): BinTree[A] = {
		if(values.isEmpty){
			return null
		}else{
			insert(values.head: A, apply(values.tail: _*): BinTree[A])
		}
	}



}
