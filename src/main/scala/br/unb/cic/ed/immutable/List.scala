package br.unb.cic.ed.immutable

trait List[+A] extends Stack[A]

case object Nil extends List[Nothing]

case class Cons[+A](head: A, tail: List[A]) extends List[A] with Stack[A]

object List{
	def insert[A](pos: Int, value: A, list: List[A]): List[A] = 
		(list,pos) match{
			case (_,x) if x <= 0	=> Cons(value, list) 
			case (Nil,_) 			=> Cons(value, Nil) 
			case (Cons(x,xs), 1) 	=> Cons(x,Cons(value,xs))
			case (Cons(x,xs),_)		=> Cons(x,insert(pos-1, value, xs))
		}

	def remove[A](pos: Int, list: List[A]): List[A] =
		(list,pos) match{
			case (Cons(x,Nil),_)	 		=> Nil
			case (Cons(x,xs),y) if y <= 0	=> xs  
			case (Cons(x,xs),_)				=> Cons(x,remove(pos-1, xs))
		}

	def append[A](first: List[A], last: List[A]): List[A] =
		first match{
			case Nil        => last
			case Cons(x,xs) => Cons(x, append(xs, last))
		}

	def take[A](qty: Int, list: List[A]): List[A] =
		(list,qty) match{
			case (Nil,_)					=> Nil
			case (Cons(x,xs),y) if y >= 1 	=> Cons(x,take(qty-1, xs))
			case (Cons(x,xs),y) if y <  1	=> Nil
		}
	
	def drop[A](qty: Int, list: List[A]): List[A] = 
		(list,qty) match{
			case (Nil,_)					=> Nil
			case (Cons(x,xs),y) if y >= 1	=> drop(qty-1,xs)
			case (Cons(x,xs),y) if y <  1	=> list
		}

	def reverse[A](list: List[A]): List[A] = 
		list match{
			case Nil => Nil
			case Cons(x,xs) => append(reverse(xs),Cons(x,Nil))
		}
	
	def compare[A](first: List[A], last: List[A]): Boolean =
		(first,last) match{
			case (Nil,Nil) 							=> true
			case (_,Nil)|(Nil,_) 					=> false
			case (Cons(x,xs),Cons(y,ys)) if x == y	=> compare(xs,ys)
			case (_,_) 								=> false
		}

	def map[A,B](list: List[A],f: A => B): List[B]=
		list match{
			case Nil => Nil
			case Cons(x,xs) => Cons(f(x),map(xs,f))
	}

	def find[A](value: A, list: List[A]): Option[Int] = {
		val it = iterator(list)
		while(!it.hasFinished()){
			if(it.see() == value){
				return Some(it.getPos())
			}else{
				it.next()
			}
		}
		return None
	}

	def elementAt[A](pos: Int, list: List[A]): Option[A] = {
		val it = iterator(list)
		while(!it.hasFinished()){
			if(it.getPos() == pos) return Some(it.see())
			it.next()
		}
		return None
	}


	def size[A](list: List[A]): Int =
		list match{
			case Nil 		=> 0
			case Cons(x,xs) => 1 + size(xs)
		}

	def iterator[A](list: List[A]): br.unb.cic.ed.iterator.Iterable[A] = new br.unb.cic.ed.iterator.IteratorImmutableList(list)

	def apply[A](values: A*): List[A] = 
		if(values.isEmpty) Nil
		else Cons(values.head: A, apply(values.tail: _*))
}
