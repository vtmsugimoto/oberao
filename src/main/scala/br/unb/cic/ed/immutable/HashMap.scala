package br.unb.cic.ed.immutable

trait HashMap[A,B]

case class Pair[A,B](key: A, value: B)

case class Hash[A,B](hash: Array[Pair[A,B]] = new Array[Pair[A,B]](16)) extends HashMap[A,B]

object HashMap{
	
		def insert[A,B](key: A, value: B, hash: Hash[A,B]): Hash[A,B] = {
			var ret: Hash[A,B] = enlarge(hash)
			if(exist(key,ret)) ret = remove(key, ret)
			ret.hash(getPos(key, ret)) = Pair(key, value)
			return ret
		}

	def exist[A,B](key: A, hash: Hash[A,B]): Boolean = {
		val pos: Int = getPos(key, hash)
		return pos >= 0 && hash.hash(pos) != null
	}

	def remove[A,B](key: A, hash: Hash[A,B]): Hash[A,B] = {
		if(exist(key, hash)) hash.hash(getPos(key, hash)) = null
		return hash
	}

	def get[A,B](key: A, hash: Hash[A,B]): B = {
		if(exist(key,hash)){
			return hash.hash(getPos(key,hash)).value
		}else{
			throw new br.unb.cic.ed.mutable.InvalidArgument("Chave "+key+" não existe no hash")
		}

	}

	def keyList[A,B](hash: Hash[A,B]): br.unb.cic.ed.immutable.List[A] = {
		var list: br.unb.cic.ed.immutable.List[A] = br.unb.cic.ed.immutable.List()
		hash.hash.foreach((x:Pair[A,B]) => if (x != null) list = br.unb.cic.ed.immutable.List.insert(br.unb.cic.ed.immutable.List.size(list),x.key,list))
		return list	
	}

	def iterator[A,B](hash: Hash[A,B]): br.unb.cic.ed.iterator.Iterable[B] = new br.unb.cic.ed.iterator.IteratorImmutableHashMap(hash,HashMap.keyList(hash))

	def getPos[A,B](key: A, hash: Hash[A,B]): Int = {
		key.hashCode.abs % hash.hash.length match{
			case pos: Int if (hash.hash(pos) != null && hash.hash(pos).key != key) => colision(pos, key, hash)
			case pos => pos
		}
	}

	private def colision[A,B](invalidPos: Int, key: A, hash: Hash[A,B]): Int = {
		(invalidPos + 1) % hash.hash.length match{
			case pos if (hash.hash(pos) != null && hash.hash(pos).key != key) => colision(pos, key, hash)
			case pos => pos
		}
	}

	def size[A,B](hash: Hash[A,B]): Int = hash.hash.count((x: Pair[A,B]) => x != null)

	def isFull[A,B](hash: Hash[A,B]): Boolean = hash.hash.length == size(hash)

	private def enlarge[A,B](hash: Hash[A,B], anyways: Boolean = false): Hash[A,B] = {
		if(isFull(hash) || anyways){
			val newHash: Array[Pair[A,B]] = new Array[Pair[A,B]]((hash.hash.length) * 2)
			hash.hash.copyToArray(newHash)
			println(newHash.length)
			return new Hash(newHash)
		}else{
			return hash
		}
	}

	def apply[A,B](pairs: Pair[A,B]*): Hash[A,B] = {
		var hash: Hash[A,B] = Hash()
		if(pairs.isEmpty) return hash
		pairs.foreach((pair: Pair[A,B]) => hash = insert(pair.key,pair.value,hash))
		return hash
	}
}
