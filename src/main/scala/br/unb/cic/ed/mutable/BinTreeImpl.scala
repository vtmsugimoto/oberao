package br.unb.cic.ed.mutable

class NodeTree[T <: Comparable[T]](val content: T, var lhs: NodeTree[T] = null, var rhs: NodeTree[T] = null) {

  def insert(v: T) {
    if(v.compareTo(content) <= 0) {
      if(lhs == null) lhs = new NodeTree(v)
      else lhs.insert(v)
    }
    else {
      if(rhs == null) rhs = new NodeTree(v)
      else rhs.insert(v)
    }
  }

  def exist(v: T) : Boolean = 
    if(v.compareTo(content) == 0) true
    else if ((v.compareTo(content) <= 0) && lhs != null) lhs.exist(v)
    else if (rhs != null) rhs.exist(v)
    else false 


  def getMax(): NodeTree[T] = {
    var max = this
    while(max.rhs != null) max = max.rhs
    return max
  }

  def getMin(): NodeTree[T] = {
    var min = this
    while(min.lhs != null) min = min.lhs
    return min
  }

}

class BinTreeImpl[T <: Comparable[T]] extends BinTree[T] {
    private var root: NodeTree[T] = null
    private var _size: Int = 0

    def insert(v: T) {
      if(root == null) root = new NodeTree(v)
      else root.insert(v) 
      _size += 1
    }

    def exist(v: T): Boolean  = if(root == null) false else root.exist(v) 

    def size(): Int = _size

    def remove(value: T): Unit = {
      if(!exist(value)){
        return 
      }else{
        _size += -1
        if(size() == 1){
          root = null
        }else{
          val it = iterator()
          while(!it.hasFinished() && it.see() != value){
            it.get()
          }
          val no = it.getNode()
          no match {
            case node: NodeTree[T] if (node.lhs == null && node.rhs == null) => {
              if(no == it.getParent().lhs){
                it.getParent().lhs = null
              }else{
                it.getParent().rhs = null
              }
            }
            case node: NodeTree[T] if (node.lhs == null && node.rhs != null) => {
              if(no == it.getParent().lhs){
                it.getParent().lhs = node.rhs
              }else{
                it.getParent().rhs = node.rhs
              }

            }
            case node: NodeTree[T] if (node.lhs != null && node.rhs == null) => {
              if(no == it.getParent().lhs){
                it.getParent().lhs = node.lhs
              }else{
                it.getParent().rhs = node.lhs
              }
            }
            case node: NodeTree[T] => {
              if(no == it.getParent().lhs){
                no.lhs.getMax().rhs = node.rhs
                it.getParent().lhs = node.lhs
              }else{
                no.lhs.getMax().rhs = node.rhs
                it.getParent().rhs = node.lhs
              }
            }
          }

        }
      }
    }

    def iterator(): br.unb.cic.ed.iterator.IteratorBinTree[T] = new br.unb.cic.ed.iterator.IteratorBinTree(this)

    def getRoot(): NodeTree[T] = root

}
