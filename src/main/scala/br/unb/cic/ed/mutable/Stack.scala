package br.unb.cic.ed.mutable

/**
  * Uma especificação do tipo pilha
  * usando a construção trait da linguagem
  * Scala.
  *
  * @author thaleslim
  */
trait Stack[T] {
  def push(value: T): Unit
  def pop(): Option[T]
  def top(): Option[T]
  def pushAll(stack: Stack[T]): Unit
  def popAll(qty: Int): Stack[T]
  def reverse(): Unit
  def iterator(): br.unb.cic.ed.iterator.Iterable[T]
  def size(): Int
}
