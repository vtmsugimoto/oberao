package br.unb.cic.ed.mutable

/**
  * Uma implementacao do tipo pilha usando
  * alocacao sequencial (um array de elementos).
  *
  * @author: thaleslim / rbonifacio
  */
class StackImpl[T](private val elements: br.unb.cic.ed.mutable.List[T]) extends Stack[T] {

  private var _size = 0

  def push(value: T): Unit = {
    elements.insert(_size, value)
    _size += 1
  }

  def pop(): Option[T] = {
    val res = elements.elementAt(_size-1)
    res match {
      case Some(v) => {
        elements.remove(_size-1)
        _size -= 1
      }
      case None => {}
    }
    return res
  }

  def top(): Option[T] = elements.elementAt(_size-1)

  def pushAll(stack: Stack[T]): Unit = {
    //stack.reverse()
    var aux: Stack[T] = new StackImpl[T](new br.unb.cic.ed.mutable.LinkedList())
    val it = stack.iterator()

    while(!it.hasFinished()){
      aux.push(it.get())
    }

    val itAux = aux.iterator()

    while(!itAux.hasFinished()){
      push(itAux.get())
    }
  }

  def popAll(qty: Int): Stack[T] = {
    var popped: Stack[T] = new StackImpl[T](new br.unb.cic.ed.mutable.LinkedList())
    var _qty = qty
    while(_qty > 0){
      pop match {
        case Some(x) => popped.push(x)
        case None => {}
      }
      _qty += -1
    }
    return popped
  }

  def reverse(): Unit = {
    var reversed: Stack[T] = new StackImpl[T](new br.unb.cic.ed.mutable.LinkedList())
    val it = iterator()

    while(!it.hasFinished()){
      reversed.push(it.get())
    }

    popAll(size())
    pushAll(reversed)
  }

  def iterator(): br.unb.cic.ed.iterator.Iterable[T] = new br.unb.cic.ed.iterator.IteratorStackImpl(this)

  def size() : Int = _size
}
