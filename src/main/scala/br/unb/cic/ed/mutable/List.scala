package br.unb.cic.ed.mutable

/**
  * Uma especifica\c c\~{a}o do tipo lista 
  * usando a constru\c c\~{a}o trait da linguagem 
  * Scala. 
  * 
  * @author rbonifacio
  */ 
trait List[T] extends Ordered[List[T]]{
  def insert(pos: Int, value: T) : Unit
  def remove(pos: Int) : Unit
  def addAll[B <: T](values: List[B]): Unit
  def removeAll(pos: Int) : Unit
  def iterator(): br.unb.cic.ed.iterator.Iterable[T]
  def reverse(): Unit
  def drop(qty: Int): List[T]
  def take(qty: Int): List[T]
  def compare(that: List[T]): Int
  def size() : Int
  def find(value: T): Option[Int]
  def elementAt(pos: Int): Option[T]
}
