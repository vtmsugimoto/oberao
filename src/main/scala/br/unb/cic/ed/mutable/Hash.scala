package br.unb.cic.ed.mutable

trait Hash[A,B]{
	def insert(key: A, value: B): Unit
	def exist(key: A): Boolean
	def remove(key: A): Unit
	def get(key: A): B 
	def keyList(): br.unb.cic.ed.immutable.List[A]
	def iterator(): br.unb.cic.ed.iterator.IteratorHashMap[A,B]
	def getPos(key: A): Int 
	def isFull(): Boolean
	def size(): Int
}
