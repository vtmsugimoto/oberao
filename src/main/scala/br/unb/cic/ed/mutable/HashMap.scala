package br.unb.cic.ed.mutable

case class Pair[A, B](key: A, value: B)

class HashMap[A, B](size: Int = 16){

	private var _size: Int = 0
	private var hash: Array[Pair[A,B]] = new Array(size)

	def insert(key: A, value: B): Unit = {
		enlarge()
		if(exist(key)) remove(key)
		hash(getPos(key)) = Pair(key, value)
		_size += 1
	}

	def exist(key: A): Boolean = { 
		val pos = getPos(key)
		return pos >= 0 && hash(pos) != null 
	}

	def remove(key: A): Unit = {
		if(exist(key)){
			val pos = getPos(key)
			hash(pos) = null
			_size += -1
		}
	}

	def get(key: A): B = {
		if(exist(key)){
			return hash(getPos(key)).value
		}else{
			throw new br.unb.cic.ed.mutable.InvalidArgument("Chave não existe no hash")
		}

	}

	def keyList(): br.unb.cic.ed.immutable.List[A] = {
		var list: br.unb.cic.ed.immutable.List[A] = br.unb.cic.ed.immutable.List()
		hash.foreach((x:Pair[A,B]) => if (x != null) list = br.unb.cic.ed.immutable.List.insert(br.unb.cic.ed.immutable.List.size(list),x.key,list))
		return list	
	}

	def iterator(): br.unb.cic.ed.iterator.IteratorHashMap[A,B] = new br.unb.cic.ed.iterator.IteratorHashMap(this,keyList())

	def getPos(key: A): Int = {
		val valueKey: Integer = key.hashCode.abs
		var pos: Int = valueKey % hash.length
		var i: Int = 0

		while(hash(pos) != null && hash(pos).key != key && i < hash.length){
			pos = (1 + pos) % hash.length
			i += 1
		}

		if(i == hash.length) pos = -pos

		return pos
	}

	private def enlarge(anyways: Boolean = false): Unit = {
		if(isFull() || anyways){
			val aux: Array[Pair[A,B]] = hash
			hash = new Array(hash.length * 2)
			_size = 0
			for(i <- 0 until aux.length){
				this.insert(aux(i).key, aux(i).value)
			}
		}
	}

	def isFull(): Boolean = hash.length - size() == 0

	def size(): Int = _size

	def length(): Int = hash.length
}
