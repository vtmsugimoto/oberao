package br.unb.cic.ed.mutable


case class NodeList[T](val value: T, var next: NodeList[T])

class LinkedList[T] extends List[T] {

  private var _size: Int = 0 
  private var head: NodeList[T] = null

  def size(): Int = _size

  private[ed] def nodeAtPosition(pos: Int): NodeList[T] = {
    var it = head
    for(i <- 0 until pos) {
      it = it.next
    }
    return it
  }

  def find(value: T): Option[Int] = {
    if(size == 0) { return None }
    var it = head
    var idx = 0
    while(it != null) {
      if(it.value == value) {
        return Some(idx); 
      }
      it = it.next
      idx += 1
    }
    return None
  }

  def elementAt(pos: Int): Option[T] = {
    if(pos < 0 || pos > _size) {
      return None
    }
    val node = nodeAtPosition(pos)
    return Some(node.value)
  }

  def insert(pos: Int, value: T){
    if(pos >=0 && pos <= _size) {
      if(pos == 0) {
        head = NodeList(value, head) 
      }
      else {
        val node = nodeAtPosition(pos-1)
        node.next = NodeList(value, node.next)
      }
      _size += 1
    }
    else {
      throw br.unb.cic.ed.mutable.InvalidArgument("Posição inalcançável")
    }
  }

  def remove(pos: Int): Unit = {
    if(pos < 0 || pos >= size) {
      throw br.unb.cic.ed.mutable.InvalidArgument("Posição inalcançável")
    }
    if(pos == 0) {
      head = head.next
    }
    else {
      val node = nodeAtPosition(pos-1)
      node.next = node.next.next
    }
    _size -= 1
  }

  def removeAll(pos: Int): Unit = {
    while(pos != _size){
      remove(pos)
    }
  }

  def addAll[B <: T](values: List[B]) : Unit = {
    val it = values.iterator()     

    while(!it.hasFinished()){
      this.insert(size(), it.get())
    }
  }

  def drop(qty: Int): List[T] = {
    val dropped: List[T] = new br.unb.cic.ed.mutable.LinkedList[T]
    val it = iterator()

    for(i <- 0 until (size() - qty)) it.next()

    while(!it.hasFinished()){
      dropped.insert(dropped.size(), it.get())
    }

    removeAll(size() - qty)

    return dropped
  }

  def take(qty: Int): List[T] = {
    val taken: List[T] = new br.unb.cic.ed.mutable.LinkedList[T]

    taken.addAll(this)
    taken.removeAll(qty)

    for(i <- 0 until qty) this.remove(0)
  
    return taken
  }

  def reverse(): Unit = {
    val reverseList: List[T] = new br.unb.cic.ed.mutable.LinkedList()
    val it = this.iterator()

    while(!it.hasFinished()){
      reverseList.insert(0, it.get())
    }

    this.removeAll(0)
    this.addAll(reverseList)
  }

  def compare(that: List[T]): Int = this.size() - that.size()

  def iterator(): br.unb.cic.ed.iterator.IteratorLinkedList[T] = {
    return new br.unb.cic.ed.iterator.IteratorLinkedList(this)
  }
}
