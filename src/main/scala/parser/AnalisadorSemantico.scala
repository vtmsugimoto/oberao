package parser

import oberon.command._
import oberon.expression._
import oberon.Environment._
import br.unb.cic.ed.immutable._
import br.unb.cic.ed.mutable.InvalidArgument
import oberon.visitor._

object AvalSem{
	//private val pp = new PrettyPrinter()	

	def checkCommand(cmd: Command): Unit = { 
		//println(cmd)
		//pp.visit(cmd)
		//println(pp.str)

		cmd match {
			case a:BlockCommand => block(a)
			case a:Declaration => declaration(a)
			case a:Assignment => assignment(a)
			case a:While => whileCom(a)
			case a:IfThen => ifthen(a)
			case a:IfThenElse => ifthenelse(a)
			case a:DecProcedure => decprocedure(a)
			case a:DecFunction => decfunction(a)
			case a:CallProcedure => callprocedure(a)
			case a:Print => print(a)
		}
	}

	def block(blck: BlockCommand): Unit = {
		push()
		List.map(blck.cmds, (a:Command) => checkCommand(a))
		pop()
	}

	def declaration(dec: Declaration): Unit = {
		if(HashMap.exist(dec.id, Stack.top(stack))) throw new InvalidArgument("Variável "+dec.id+" já declarada.")
		if(existCall(dec.id)) throw new InvalidArgument(dec.id+" já declarado como função.")
		(dec.tipo,dec.expression.eval()) match {
			case ("Inteiro",a:IntValue) => {}
			case ("Inteiro",a:Undefined) => {}
			case ("Bool",a:BoolValue) => {}
			case ("Bool",a:Undefined) => {}
			case (_,_) => throw new InvalidArgument("Expressão de tipo errado.")
		}
		dec.run()
	}


	def assignment(ass: Assignment): Unit = {
		if(!existVar(ass.id))
			throw new InvalidArgument("Variável "+ass.id+" não declarada.")
		expressao(ass.expression)
		ass.run()
	}

	def whileCom(whi: While): Unit = {
		condition(whi.cond)
		checkCommand(whi.command)
	}

	def ifthen(ifi: IfThen): Unit = {
		condition(ifi.cond)
		checkCommand(ifi.command)
	}

	def ifthenelse(ifi: IfThenElse): Unit = {
		condition(ifi.cond)
		checkCommand(ifi.commandTrue)
		checkCommand(ifi.commandFalse)
	}

	def decprocedure(pro: DecProcedure): Unit = {
		if(existVar(pro.id)) throw new InvalidArgument(pro.id+" já declarado como variável.")
		if(existCall(pro.id)) throw new InvalidArgument(pro.id+" já declarado como procedimento.")
    	pro.run()		
	}

	def decfunction(fun: DecFunction): Unit = {
		if(existVar(fun.id)) throw new InvalidArgument(fun.id+" já declarado como variável.")
		if(existCall(fun.id)) throw new InvalidArgument(fun.id+" já declarado como função.")
		fun.run()
	}

	def callprocedure(call: CallProcedure): Unit = {
	    val foo = getCall(call.id)

		if(Stack.size(call.input) != List.size(foo.asInstanceOf[Procedure].parametros))
			throw new InvalidArgument("Quantidade de argumentos inválida para chamada da função "+call.id+".")

	    var parEn: Stack[Expression] = Stack()
	    var aux: Stack[Expression] = Stack()

	    aux = Stack.pushAll(call.input, aux)

	    while(Stack.size(parEn) != Stack.size(call.input)){
	      parEn = Stack.push(Stack.top(aux).eval(),parEn)
	      //println(Stack.top(parEn))
	      aux = Stack.pop(aux)
	    }

	    startCall()

	    if(Stack.size(parEn) > 0){
	    	parEn = Stack.reverse(parEn)
		    List.map(foo.asInstanceOf[Procedure].parametros,
		      (par:Parametro) => {
		        var dec = new Declaration(par.tipo, par.nome, Stack.top(parEn))
		        //println(par.nome)
		        dec.run()
		        aux = Stack.pop(parEn)
		    })
	    }

	    checkCommand(foo.asInstanceOf[Procedure].comandos)

	    endCall()
	}

	def print(print: Print): Unit = {

	}

	private def expressao(exp: Expression): Unit =
		exp match {
			case a:IntValue => {}
			case a:BoolValue => {}
			case a:Undefined => {}
			case a:ReadInt => {}
			case a:ReadBool => {}
			case a:VarRef => reference(a)
			case a:CallFunction => callfunction(a)
			case _ => binexpression(exp)
		}

	private def reference(ref: VarRef): Unit = {
		expressao(ref)
		if(!existVar(ref.id))
			throw new InvalidArgument("Variável "+ref.id+" não declarada.")
	}

	private def binexpression(op: Expression): Unit = {
		val exp = op match {
			case a:OutOfScope => a.value
			case _ => {}
		}
		op match {
			case a:Operation => {
				(getOutOfScope(a.lhs.eval()),getOutOfScope(a.rhs.eval())) match {
					case (a:IntValue,b:IntValue) => {}
					case (_,_) => throw new InvalidArgument("Expressão espera valor int, mas não obteve.")
				}
			}
			case a:Comparation => {
				(getOutOfScope(a.lhs.eval()),getOutOfScope(a.rhs.eval())) match {
					case (a:IntValue,b:IntValue) => {}
					case (_,_) => throw new InvalidArgument("Expressão espera valor int, mas não obteve.")
				}
			}
			case a:Logic => {
				(getOutOfScope(a.lhs.eval()),getOutOfScope(a.rhs.eval())) match {
					case (a:BoolValue,b:BoolValue) => {}
					case (_,_) => throw new InvalidArgument("Expressão espera valor bool, mas não obteve.")
				}
			}
			case _ => {}
		}
	}

	private def callfunction(call: CallFunction): Unit = {

	}

	private def condition(cond: Expression): Unit = {
		cond match {
			case a:Comparation => expressao(a)
			case a:Logic => expressao(a)
			case _ => throw new InvalidArgument("Condição não retona valor válido.")
		}
	}

	private def existCall(id: String): Boolean = {
		val hash: Hash[String,Chamavel] = Hash()
		HashMap.exist(id, hash)
	}

	private def existVar(id: String): Boolean = {
		var pilha: Stack[Hash[String,Value]] = Stack()
		pilha = Stack.pushAll(stack, pilha)
		while(Stack.size(pilha) > 0 && !HashMap.exist(id, Stack.top(pilha))){
			pilha = Stack.pop(pilha)
		}
		Stack.size(pilha) != 0
	}

	private def getOutOfScope(outofscope: Value): Value = {
		outofscope match {
			case a:OutOfScope => a.value
			case _	=> outofscope
		}
	}
}