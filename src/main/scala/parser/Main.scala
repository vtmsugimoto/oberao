
package parser

import oberon._
import br.unb.cic.ed.immutable.List
import oberon.command._
import oberon.expression._
import oberon.visitor._

object Main extends App{
	def main(): Unit = {
		if(args.length != 0){
			val arq = args(0)
			val src = scala.io.Source.fromFile(arq)
			val lines = try src.mkString finally src.close()
			val compilado = Compiler(lines)
			//println(compilado+"\n\n")
			//val blocoPrograma = new BlockCommand(parserToList(compilado))
			val blocoprograma = parserToBlock(compilado)
			AvalSem.checkCommand(blocoprograma)
			val programa = new OberonProgram(blocoprograma)

			println("\n\n")
			val pp = new PrettyPrinter()
			List.map(blocoprograma.asInstanceOf[BlockCommand].cmds, (a:Command) =>{
					pp.visit(a)
					println(pp.str)
				})

			println("\n\nEXECUÇAO: \n\n")
			Environment.clear()
			programa.run()
			println("\nFIM\n\n")
		}
	}
	def parserToBlock(parser: Either[ErroComp, Command]): Command = {
		var continue: Boolean = true
		var lista: List[BlockCommand] = List()
		parser match {
			case Right(x:oberon.command.BlockCommand) => {
				/*println("Agora vai a lista:\n")
				List.map(x.cmds, (a:oberon.command.Command) => {
					a match {
						case b:Assignment => println(b.expression)
						case _ => println(a)
					}
					}
				)*/
				return x
			}
			case _ => throw new br.unb.cic.ed.mutable.InvalidArgument("Falha em parserToBlock")
		}
	}
	
	main()
}

object Compiler{
	def apply(code: String): Either[ErroComp, oberon.command.Command] = {
		for{
			tokens <- Lexer(code).right
			arvore <- OberonParser(tokens).right
		} yield arvore
	}
}
