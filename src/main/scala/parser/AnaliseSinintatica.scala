package parser

import oberon._
import scala.util.parsing.combinator._
import scala.util.parsing.input._
import parser._
import oberon.expression._
import oberon.command._
import br.unb.cic.ed.immutable._

sealed trait ArvoreSintatica

case object Mais extends ArvoreSintatica
case object Menos extends ArvoreSintatica
case object Vezes extends ArvoreSintatica
case object Dividido extends ArvoreSintatica
case object Modulo extends ArvoreSintatica

case object E extends ArvoreSintatica
case object Ou extends ArvoreSintatica
case object Negacao extends ArvoreSintatica

case object Maior extends ArvoreSintatica
case object Menor extends ArvoreSintatica
case object Diferente extends ArvoreSintatica
case object MaiorIgual extends ArvoreSintatica
case object MenorIgual extends ArvoreSintatica
case object Equivalente extends ArvoreSintatica

case class ParserError(msg: String) extends ErroComp

object OberonParser extends Parsers{
	override type Elem = Token
	var eusoqueroquerfuncione = false

	private def identificador: Parser[Identificador] = accept("identificador", {case a:Identificador => a})

	private def literal: Parser[Literal] = accept("string literal", {case lit @ Literal(name) => lit})

	private def numero: Parser[Numero] = accept("numero", {case num @ Numero(name) => num})

	def program: Parser[oberon.command.Command] = phrase(comando)

	def operador: Parser[ArvoreSintatica] = {
		val mais = Plus ^^ (_ => Mais)
		val menos = Minus ^^ (_ => Menos)
		val vezes = Times ^^ (_ => Vezes)
		val dividido = Div ^^ (_ => Dividido)
		val modulo = Mod ^^ (_ => Modulo)

		mais|menos|vezes|dividido|modulo
	}

	def comparador: Parser[ArvoreSintatica] = {
		val e = And ^^ (_ => E)
		val ou = Or ^^ (_ => Ou)

		val maiorigual = Gte ^^ (_ => MaiorIgual)
		val menorigual = Lte ^^ (_ => MenorIgual)
		val maior = Gt ^^ (_ => Maior)
		val menor = Lt ^^ (_ => Menor)
		val diferente = Dif ^^ (_ => Diferente)
		val equivalente = Equiv ^^ (_ => Equivalente)
		
		e|ou|maiorigual|menorigual|maior|menor|diferente|equivalente		
	}

	def expressao: Parser[oberon.expression.Expression] = {
		//val neguni = Neg ~ expressao ^^ {case Neg ~ exp => BoolNeg(exp)}
		
		val uni = (numero|Verdadeiro|Falso|identificador) ^^ {
			case Numero(name) => new IntValue(name.toInt)
			case Verdadeiro => new BoolValue(true)
			case Falso => new BoolValue(false)
			case Identificador(name) => new VarRef(name)
			case _ => throw new br.unb.cic.ed.mutable.InvalidArgument("Isso não vai acontecer... eu espero...")
		}
		val bin = uni ~ operador ~ expressao ^^ {
			case exp1 ~ op ~ exp2 => 
				{
					op match {
						case Mais => new oberon.expression.AddExpression(exp1, exp2)
						case Menos => new SubExpression(exp1, exp2)
						case Vezes => new MulExpression(exp1, exp2)
						case Dividido => new DivExpression(exp1, exp2)
						case Modulo => new ModExpression(exp1, exp2)
						case _ => throw new br.unb.cic.ed.mutable.InvalidArgument("Isso não vai acontecer... eu espero...")
					}
				}
			}
	
		val boolbin = uni ~ comparador ~ expressao ^^ {
			case exp1 ~ op ~ exp2 =>
				{
					op match {
						case E => new AndExpression(exp1, exp2)																 
						case Ou => new OrExpression(exp1, exp2)
						case Maior => new GtExpression(exp1, exp2)
						case Menor => new LtExpression(exp1, exp2)
						case MaiorIgual => new GteExpression(exp1, exp2)
						case MenorIgual => new LteExpression(exp1, exp2)
						case Diferente => new DifExpression(exp1, exp2)
						case Equivalente => new EqExpression(exp1, exp2) 	
						case _ => throw new br.unb.cic.ed.mutable.InvalidArgument("Isso não vai acontecer... eu espero...")
					}
				}
			}
		val chamada = Chamada ~ identificador ~ AbrePar ~ rep(expressao) ~ FechaPar ^^ {
			case _ ~ Identificador(name) ~ _ ~ listexp ~ _ => {
				var lista:List[Expression] = List()
				if(!listexp.isEmpty){
					listexp.foreach(elem => lista = List.insert(List.size(lista), elem, lista))
				}
				new CallFunction(name, lista)
			}
		}
		val lerinteiro = LerInteiro ~ AbrePar ~ FechaPar ^^ {
			case _ ~ _ ~ _ => new ReadInt()
		}
		val lerbool = LerBool ~ AbrePar ~ FechaPar ^^ {
			case _ ~ _ ~ _ => new ReadBool()
		}		
			//neguni
		chamada|lerinteiro|lerbool|bin|boolbin|uni
	}

	def comando: Parser[oberon.command.Command] = {
		val atribuicao = identificador ~ Equal ~ expressao ^^ {
			case Identificador(name) ~ Equal ~ exp => new Assignment(name, exp)
		}
		val declaracao = (Inteiro|Bool) ~ identificador ~ opt(Equal ~ expressao) ^^ {
			case tipo ~ Identificador(name) ~ Some(Equal ~ exp) => new Declaration(tipo.toString, name, exp)
			case tipo ~ Identificador(name) ~ None => new Declaration(tipo.toString, name, new Undefined())
		}
		val bloco = Begin ~ rep1(comando) ~ End ^^{
			case Begin ~ list ~ End => {
				val cmd = list.asInstanceOf[scala.collection.immutable.List[Command]].head
				var lista:List[oberon.command.Command] = List(cmd)
				list.asInstanceOf[scala.collection.immutable.List[Command]].tail.foreach(a => {lista = List.insert(List.size(lista), a, lista)})
				new oberon.command.BlockCommand(lista)																								
			}
		}
		val enquanto = Enquanto ~ AbrePar ~ expressao ~ FechaPar ~ comando ^^ {
			case _ ~ _ ~ cond ~ _ ~ cmd => new oberon.command.While(cond, cmd)
		}
		val seentaonao = Se ~ AbrePar ~ expressao ~ FechaPar ~ Entao ~ comando ~ Contrario ~ comando ^^ {
			case _ ~ _ ~ cond ~ _ ~ _ ~ cmd1 ~ _ ~ cmd2 => new oberon.command.IfThenElse(cond, cmd1, cmd2)
		}
		val seentao = Se ~ AbrePar ~ expressao ~ FechaPar ~ Entao ~ comando ^^ {
			case _ ~ _ ~ cond ~ _ ~ _ ~ cmd => new oberon.command.IfThen(cond, cmd)
		}
		val impressao = Imprimir ~ AbrePar ~ expressao ~ FechaPar ^^ {
			case _ ~ _ ~ exp ~ _ => new oberon.command.Print(exp)
		}
		val declaracaoprocedimento = Procedimento ~ identificador ~ AbrePar ~ rep((Inteiro|Bool) ~ identificador) ~ FechaPar ~ comando ^^ {
			case Procedimento ~ Identificador(name) ~ _ ~ list ~ _  ~ cmds=> {
				var listaPar:List[Parametro] = List()

				list.foreach(elem => elem match {
					case (Inteiro ~ Identificador(name)) => {
						listaPar = List.insert(List.size(listaPar), Parametro("int", name), listaPar)
					}
					case (Bool ~ Identificador(name)) => {
						listaPar = List.insert(List.size(listaPar), Parametro("bool", name), listaPar)
					}
				})
				new DecProcedure(name, new Procedure(listaPar, cmds))
			}
		}
		val declaracaofuncao = Funcao ~ (Inteiro|Bool) ~ identificador ~ AbrePar ~ rep((Inteiro|Bool) ~ identificador) ~ FechaPar ~ comando ^^ {
			case _ ~ tipo ~ Identificador(name) ~ _ ~ list ~ _ ~ comando => {
				var listaPar:List[Parametro] = List()

				list.foreach(elem => elem match {
					case (Inteiro ~ Identificador(name)) => {
						listaPar = List.insert(List.size(listaPar), Parametro("int", name), listaPar)
					}
					case (Bool ~ Identificador(name)) => {
						listaPar = List.insert(List.size(listaPar), Parametro("bool", name), listaPar)
					}
				})
				new DecFunction(name, new Function(listaPar, comando, tipo.toString))
			}
		}
		val chamada = Execucao ~ identificador ~ AbrePar ~ rep(expressao) ~ FechaPar ^^ {
			case _ ~ Identificador(name) ~ _ ~ listexp ~ _ => {
				var pilha:Stack[Expression] = Stack()
				if(!listexp.isEmpty){
					listexp.foreach(elem => {println("Elem: "+elem);pilha = Stack.push(elem, pilha)})
					pilha = Stack.reverse(pilha)
				}
				new CallProcedure(name, pilha)
			}
		}
		val retorno = Retorno ~ expressao ^^ {
			case Retorno ~ exp => new Return(exp)
		}
		retorno|chamada|atribuicao|declaracaofuncao|declaracao|bloco|enquanto|seentaonao|seentao|impressao|declaracaoprocedimento
	}

	def apply(tokens: Seq[Token]): Either[ParserError, oberon.command.Command] = {
		val reader = new TokenReader(tokens)
		program(reader) match {
			case NoSuccess(msg, next) => Left(ParserError(msg))
			case Success(result, next) => Right(result)
		}
	}
}

class TokenReader(tokens: Seq[Token]) extends Reader[Token]{
	override def first: Token = tokens.head
	override def atEnd: Boolean = tokens.isEmpty
	override def pos: Position = NoPosition
	override def rest: Reader[Token] = new TokenReader(tokens.tail)
}
