package parser

import scala.util.parsing.combinator._

sealed trait Token
trait ErroComp

case class ErroLexico(msg: String) extends ErroComp

case class Identificador(name: String) extends Token
case class Literal(name: String) extends Token
case class Numero(name: String) extends Token
//case class Delimitador(name: String) extends Token
case object Chamada extends Token
case object Execucao extends Token
case object Funcao extends Token
case object Procedimento extends Token
case object Retorno extends Token
case object Begin extends Token
case object End extends Token
case object Plus extends Token
case object Minus extends Token
case object Times extends Token
case object Mod extends Token
case object Div extends Token
case object Verdadeiro extends Token
case object Falso extends Token
case object And extends Token
case object Or extends Token
case object Gt extends Token
case object Lt extends Token
case object Dif extends Token
case object Gte extends Token
case object Lte extends Token
case object Equiv extends Token
case object Neg extends Token
case object Inteiro extends Token
case object Bool extends Token
case object Equal extends Token
case object Enquanto extends Token
case object Se extends Token
case object Entao extends Token
case object Contrario extends Token
case object AbrePar extends Token
case object FechaPar extends Token
case object Imprimir extends Token
case object LerInteiro extends Token
case object LerBool extends Token

object Lexer extends RegexParsers{
	override def skipWhitespace = true
	override val whiteSpace = "[ \n\t\r\f]+".r

	def identificador: Parser[Identificador] = {
		"[a-zA-Z_][a-zA-Z0-9_]*".r ^^ {
			str => Identificador(str)
		}
	}
	def literal: Parser[Literal] = {
		""""[^"]*"""".r ^^ {
			str => {
				val content = str.substring(1,str.length-1)
				Literal(content)
			}
		}
	}
	def numero: Parser[Numero] = {
		"[0-9][0-9]*".r ^^ {
			str => Numero(str)
		}
	}
	def chamada = "call" ^^ (_ => Chamada)
	def execucao = "run" ^^ (_ => Execucao)
	def funcao = "function" ^^ (_ => Funcao)
	def procedimento = "procedure" ^^ (_ => Procedimento)
	def retorno = "return" ^^ (_ => Retorno)
	def begin = "begin" ^^ (_ => Begin)
	def end = "end" ^^ (_ => End)
	def plus = "+" ^^ (_ => Plus)
	def minus = "-" ^^ (_ => Minus)
	def times = "*" ^^ (_ => Times)
	def mod = "%" ^^ (_ => Mod)
	def div = "/" ^^ (_ => Div)
	def verdadeiro = "true" ^^ (_ => Verdadeiro)
	def falso = "false" ^^ (_ => Falso)
	def and = "&&" ^^ (_ => And)
	def or = "||" ^^ (_ => Or)
	def gt = ">" ^^ (_ => Gt)
	def lt = "<" ^^ (_ => Lt)
	def dif = "!=" ^^ (_ => Dif)
	def gte = ">=" ^^ (_ => Gte)
	def lte = "<=" ^^ (_ => Lte)
	def equiv = "==" ^^ (_ => Equiv)
	def neg = "!" ^^ (_ => Neg)
	def inteiro = "int" ^^ (_ => Inteiro)
	def bool = "bool" ^^ (_ => Bool)
	def equal = "=" ^^ (_ => Equal)
	def enquanto = "while" ^^ (_ => Enquanto)
	def se = "if" ^^ (_ => Se)
	def entao = "then" ^^ (_ => Entao)
	def contrario = "else" ^^ (_ => Contrario)
	def abrePar = "(" ^^ (_ => AbrePar)
	def fechaPar = ")" ^^ (_ => FechaPar)
	def imprimir = "print" ^^ (_ => Imprimir)
	def lerinteiro = "readInt" ^^ (_ => LerInteiro)
	def lerbool = "readBool" ^^ (_ => LerBool)

	def tokens: Parser[List[Token]] = {
		phrase(rep1(execucao|chamada|retorno|funcao|procedimento|lerinteiro|lerbool|imprimir|abrePar|fechaPar|begin|end|verdadeiro|falso|enquanto|se|entao|contrario
		|inteiro|bool|plus|minus|times|mod|div|and|or|gte|lte|gt|lt|dif|equiv
		|neg|equal|literal|identificador|numero)) ^^ {
			variosTokens => variosTokens
		}
	}

	def apply(codigo: String): Either[ErroLexico, List[Token]] = {
		parse(tokens, codigo) match {
			case NoSuccess(msg, next) => Left(ErroLexico(msg))
			case Success(result, next) => {
				//print(result);println("\n");
				Right(result)
			}
		}

	}
}










