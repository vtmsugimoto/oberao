package aqui

import scala.util.parsing.combinator._

trait Token
trait ErroComp

case class ErroLexico(msg: String) extends ErroComp

case class Identificador(name: String) extends Token
case class Literal(name: String) extends Token
case class Numero(name: String) extends Token
case class Delimitador(name: String) extends Token
case object Begin extends Token
case object End extends Token
case object Plus extends Token
case object Minus extends Token
case object Times extends Token
case object Mod extends Token
case object Div extends Token
case object Verdadeiro extends Token
case object Falso extends Token
case object And extends Token
case object Or extends Token
case object Gt extends Token
case object Lt extends Token
case object Dif extends Token
case object Gte extends Token
case object Lte extends Token
case object Equiv extends Token
case object Neg extends Token
case object Inteiro extends Token
case object Bool extends Token
case object Equal extends Token
case object Enquanto extends Token
case object Se extends Token
case object Entao extends Token
case object Contrario extends Token

object lexico extends RegexParsers{
	override def skipWhitespace = true
	override val whiteSpace = "[ \n\t\r\f]+".r

	def identificador: Parser[Identificador] = {
		"[a-zA-Z_][a-zA-Z0-9_]*".r ^^ {
			str => Identificador(str)
		}
	}
	def literal: Parser[Literal] = {
		""""[^"]*"""".r ^^ {
			str => {
				val content = str.substring(1,str.length-1)
				Literal(content)
			}
		}
	}
	def numero: Parser[Numero] = {
		"[0-9][0-9]*".r ^^ {
			str => Numero(str)
		}
	}
	def delimitador: Parser[Delimitador] = {
		"[([{}])]".r ^^ {
			str => Delimitador(str)
		}
	}
	def begin = "begin" ^^ (_ => Begin)
	def end = "end" ^^ (_ => End)
	def plus = "+" ^^ (_ => Plus)
	def minus = "-" ^^ (_ => Minus)
	def times = "*" ^^ (_ => Times)
	def mod = "%" ^^ (_ => Mod)
	def div = "/" ^^ (_ => Div)
	def verdadeiro = "true" ^^ (_ => Verdadeiro)
	def falso = "false" ^^ (_ => Falso)
	def and = "&&" ^^ (_ => And)
	def or = "||" ^^ (_ => Or)
	def gt = ">" ^^ (_ => Gt)
	def lt = "<" ^^ (_ => Lt)
	def dif = "!=" ^^ (_ => Dif)
	def gte = ">=" ^^ (_ => Gte)
	def lte = "<=" ^^ (_ => Lte)
	def equiv = "==" ^^ (_ => Equiv)
	def neg = "!" ^^ (_ => Neg)
	def inteiro = "int" ^^ (_ => Inteiro)
	def bool = "bool" ^^ (_ => Bool)
	def equal = "=" ^^ (_ => Equal)
	def enquanto = "while" ^^ (_ => Enquanto)
	def se = "if" ^^ (_ => Se)
	def entao = "then" ^^ (_ => Entao)
	def contrario = "else" ^^ (_ => Contrario)

	def tokens: Parser[List[Token]] = {
		phrase(rep1(begin|end|verdadeiro|falso|enquanto|se|entao|contrario
		|inteiro|bool|plus|minus|times|mod|div|and|or|gt|lt|dif|gte|lte|equiv
		|neg|equal|literal|identificador|numero|delimitador)) ^^ {
			variosTokens => variosTokens
		}
	}

	def apply(codigo: String): Either[ErroLexico, List[Token]] = {
		parse(tokens, codigo) match {
			case NoSuccess(msg, next) => Left(ErroLexico(msg))
			case Success(result, next) => Right(result)
		}

	}
}










