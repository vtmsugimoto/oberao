package oberon.visitor

import oberon.expression._
import br.unb.cic.ed.immutable.List
import oberon.command._


class PrettyPrinter extends Visitor {
  var str = ""
  private var tabulacao = 0

  def visit(e: Expression): Unit = {
    e match {
      case c: ReadInt        => visit(c)
      case c: ReadBool       => visit(c)
      case c: AddExpression  => visit(c)
      case c: SubExpression  => visit(c)
      case c: ModExpression  => visit(c)
      case c: DivExpression  => visit(c)
      case c: MulExpression  => visit(c)
      case c: OrExpression   => visit(c)
      case c: AndExpression  => visit(c)
      case c: CallFunction   => visit(c)
      case c: Undefined      => visit(c)
      case c: OutOfScope     => visit(c)
      case c: IntValue       => visit(c)
      case c: BoolValue      => visit(c)
      case c: NotExpression  => visit(c)
      case c: GtExpression   => visit(c)
      case c: LtExpression   => visit(c)
      case c: DifExpression  => visit(c)
      case c: GteExpression  => visit(c)
      case c: LteExpression  => visit(c)
      case c: EqExpression   => visit(c)
      case c: VarRef         => visit(c)
    }
  }

  def visit(e: Command): Unit = {
    e match{
      case c: BlockCommand   => visit(c)
      case c: Return         => visit(c)
      case c: Declaration    => visit(c)
      case c: Assignment     => visit(c)
      case c: While          => visit(c) 
      case c: IfThen         => visit(c)
      case c: IfThenElse     => visit(c)
      case c: DecProcedure   => visit(c) 
      case c: DecFunction    => visit(c)
      case c: CallProcedure  => visit(c)
      case c: Print          => visit(c)
    }
  }

  def visit(v: Value): Unit = visit(v.asInstanceOf[Expression])

  def visit(e: ReadInt)       : Unit = { str = "readInt()"}
  def visit(e: ReadBool): Unit = { str = "readBool()"}

  def visit(e: AddExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "+" + r + ")"
  }
  def visit(e: SubExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "-" + r + ")"
  }
  def visit(e: ModExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "%" + r + ")"
  }
  def visit(e: DivExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "/" + r + ")"
  }
  def visit(e: MulExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "*" + r + ")"
  }

  def visit(e: OrExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "||" + r + ")"
  }
  def visit(e: AndExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "&&" + r + ")"
  }
  
  def visit(e: CallFunction) : Unit = {
    var linput = ""
    List.map(e.input, (a:Expression) =>{
        visit(a)
        linput += str + ", " 
      })
    linput = linput.take(linput.size - 2)
    str = "Call " + e.id + "(" + linput + ")"
  }

  def visit(e: Undefined)     : Unit = { str = "undefined" } 
  def visit(e: OutOfScope)    : Unit = visit(e.value)
  def visit(e: IntValue)      : Unit = { str = e.value.toString() }
  def visit(e: BoolValue)     : Unit = { str = e.value.toString() }
  def visit(e: NotExpression) : Unit = {
    visit(e.exp)
    str = "!" + str
  }

  def visit(e: GtExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + ">" + r + ")"
  } 
  def visit(e: LtExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "<" + r + ")"
  }
  def visit(e: DifExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "!=" + r + ")"
  }
  def visit(e: GteExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + ">=" + r + ")"
  }
  def visit(e: LteExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "<=" + r + ")"
  }
  def visit(e: EqExpression) : Unit = {
    visit(e.lhs)
    val l = str
    visit(e.rhs)
    val r = str
    str = "(" + l + "==" + r + ")"
  }

  def visit(e: VarRef)        : Unit = {
    visit(e.eval())
    str = "(" + e.id + " = " + str + ")"
  }

  def visit(c: BlockCommand)  : Unit = {
    var cmdstr: String = ""
    tabulacao += 1
    List.map(c.cmds, (a:Command) => {
        visit(a)
        cmdstr += this.str + "\n"
        for(i <- 0 until tabulacao){
          cmdstr = cmdstr.concat("  ")
        }
      })
    cmdstr = cmdstr.take(cmdstr.size - (1 + 2*tabulacao))
    tabulacao += -1
    str = cmdstr
  }

  def visit(c: Return)  : Unit = {
    visit(c.ret)
    str = "Retorno " + str
  }

  def visit(c: Declaration)  : Unit = {
    visit(c.expression)
    str = c.tipo + " " + c.id + " = " + str
  }
  def visit(c: Assignment)    : Unit = {
    visit(c.expression)
    str = c.id + " = " + str
  }
  def visit(c: While)         : Unit = {
    visit(c.cond)
    val cnd = str
    visit(c.command)
    val cmd = str
    str = "While(" + cnd + ")\n"
    tabulacao += 1
    tabstr()
    tabulacao += -1
    str += cmd
  }
  def visit(c: IfThen)  : Unit = {
    visit(c.cond)
    val cnd = str
    visit(c.command)
    val cmd = str
    str = "If(" + cnd + ")\n"
    tabulacao += 1
    tabstr()    
    tabulacao += -1
    str += cmd    
  }
  def visit(c: IfThenElse)  : Unit = {
    visit(c.cond)
    val cnd = str
    visit(c.commandTrue)
    val cmdT = str
    visit(c.commandFalse)
    val cmdF = str
    str = "If(" + cnd + ")\n"
    tabulacao += 1
    tabstr()    
    str += cmdT + "\n"
    tabulacao += -1
    tabstr()
    str += "Else\n"
    tabulacao += 1
    tabstr()
    str += cmdF
    tabulacao += -1
  }
  def visit(c: DecProcedure)  : Unit = {
    var lpar = ""
    List.map(c.foo.parametros, (a:Parametro) =>{
        lpar += a.tipo + " " + a.nome + ", "
      })
    lpar = lpar.take(lpar.size - 2)
    visit(c.foo.comandos)
    str = "Procedure " + c.id + "(" + lpar + ")\n" + str
  }
  def visit(c: DecFunction): Unit = {
    var lpar = ""
    List.map(c.foo.parametros, (a:Parametro) =>{
        lpar += a.tipo + " " + a.nome + ", "
      })
    lpar = lpar.take(lpar.size - 2)
    visit(c.foo.comandos)
    str = "Function " + c.foo.retorno + " " + c.id + "(" + lpar + ")\n" + str
  }
  def visit(c: CallProcedure)  : Unit = {
    str = "Run " + c.id + "()"
  }
  def visit(c: Print)         : Unit = {
    visit(c.exp)
    str = "Print(" + str + ")"
  }
  private def tabstr(): Unit = {
    for(i <- 0 until tabulacao){
          str = str.concat("  ")
    }
  }
}
