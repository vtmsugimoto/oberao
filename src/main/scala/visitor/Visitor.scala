package oberon.visitor

import oberon.expression._ 
import oberon.command._


trait Visitable {
  def accept(v : Visitor) : Unit 
}

trait Visitor {
  def visit(v: Value): Unit
  def visit(e: Expression): Unit
  def visit(c: Command): Unit

  def visit(e: ReadInt)       : Unit
  def visit(e: ReadBool): Unit

  def visit(e: AddExpression) : Unit
  def visit(e: SubExpression) : Unit
  def visit(e: ModExpression) : Unit
  def visit(e: DivExpression) : Unit
  def visit(e: MulExpression) : Unit

  def visit(e: OrExpression) : Unit
  def visit(e: AndExpression) : Unit
  
  def visit(e: CallFunction) : Unit

  def visit(e: Undefined)     : Unit
  def visit(e: OutOfScope) : Unit
  def visit(e: IntValue)      : Unit
  def visit(e: BoolValue)     : Unit
  def visit(e: NotExpression) : Unit

  def visit(e: GtExpression) : Unit
  def visit(e: LtExpression) : Unit
  def visit(e: DifExpression) : Unit
  def visit(e: GteExpression) : Unit
  def visit(e: LteExpression) : Unit
  def visit(e: EqExpression) : Unit

  def visit(e: VarRef)        : Unit 

  def visit(c: BlockCommand)  : Unit
  def visit(c: Return)  : Unit
  def visit(c: Declaration)  : Unit
  def visit(c: Assignment)    : Unit
  def visit(c: While)         : Unit
  def visit(c: IfThen)  : Unit
  def visit(c: IfThenElse)  : Unit
  def visit(c: DecProcedure)  : Unit
  def visit(c: DecFunction): Unit
  def visit(c: CallProcedure)  : Unit
  def visit(c: Print)         : Unit
}
