package oberon.expression

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.GivenWhenThen
import org.scalatest.BeforeAndAfter


class TestExpression extends FlatSpec with Matchers with GivenWhenThen with BeforeAndAfter {

  behavior of "simple expressions"

  it should "return value 5 in IntValue(5).eval" in {
    val val5 = new IntValue(5)

    val5.eval() should be (IntValue(5)) 
  }

  it should "never gonna give you up" in {
  	val par1 = br.unb.cic.ed.immutable.Pair("int", br.unb.cic.ed.immutable.List("x"))
  	val fun = oberon.expression.Function(br.unb.cic.ed.immutable.HashMap(par1), "int", new oberon.command.Print(new VarRef("x")))
  	val dec = new oberon.command.DecFunction("printa", fun)
  	dec.run()
  	val exec = new oberon.expression.CallFunction("printa", br.unb.cic.ed.immutable.List(new oberon.expression.IntValue(2)))
  	exec.eval() should be (IntValue(0))
  }
}
